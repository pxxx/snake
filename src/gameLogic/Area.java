package gameLogic;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

class Area extends Dimension {
  private final Position position;

  private final Random random = new Random();

  public int getX() {
    return position.getX();
  }

  public int getY() {
    return position.getY();
  }

  public Area(Dimension d, Position position) {
    super(d);
    this.position = position;
  }

  public Area(final int width, final int height, final Position position) {
    super(width, height);
    this.position = position;
  }

  public Area(int width, int height, int positionX, int positionY) {
    super(width, height);
    this.position = new Position(positionX, positionY);
  }

  public Area(Dimension d, int positionX, int positionY) {
    super(d);
    this.position = new Position(positionX, positionY);
  }

  public List<Position> getAllPositions() {
    final ArrayList<Position> positions = new ArrayList<>();
    for (int x = position.getX(); x <= position.getX() + this.getWidth() - 1; x++)
      for (int y = position.getY(); y <= position.getY() + this.getHeight() - 1; y++) {
        positions.add(new Position(x, y));
      }
    return positions;
  }

  public Position getRandomFreePosition() {
    final List<Position> freePositions =  getAllPositions().stream().filter(Position::isFree)
            .collect(Collectors.toList());
    if (freePositions.isEmpty()) return null;
    return freePositions.get(random.nextInt(freePositions.size()));
  }

  public Position getRandomPosition() {
    if (this.getWidth() <= 0 | this.getHeight() <= 0) return null;
    return new Position(position.getX() + ((width-1 == 0)? 0 : random.nextInt(width-1)), position.getY()
            + ((height-1 == 0)? 0 : random.nextInt(height-1)));
  }
}
