package gameLogic;

import java.util.List;

public class Field {

  public static GameObject[][] cells = new GameObject[1][1];

  public static void init(int width, int height){
    cells = new GameObject[width][height];
  }

  static void setCell(Position position, GameObject object) {
    cells[position.getX()][position.getY()] = object;
  }

  static void setCells(Position[] positions, GameObject object) {
    for (Position e : positions) setCell(e, object);
  }

  static void setCells(List<Position> positions, GameObject object) {
    for (Position e : positions) setCell(e, object);
  }

  public static int getWidth() {
    return cells.length;
  }

  public static int getHeight() {
    return cells[0].length;
  }

  static Area getCenter() {
    Position position = new Position((int)Math.round(getWidth() * 0.1), (int) Math.round(getHeight() * 0.1));
    return new Area(getWidth() - (position.getX() * 2), getHeight() - (position.getY() * 2), position);
  }

  public static Area getArea() {
    return new Area(getWidth(), getHeight(), 0, 0);
  }

  public static Position getRandomFreePosition() {
    return getArea().getRandomFreePosition();
  }

  public static int countGameObject(GameObject object) {
    int count = 0;
    for (GameObject[] column : cells)
      for (GameObject e : column)
        if (e == object)
          count++;
    return count;
  }

  public static Boolean isPositionOnLeftWall(Position position) {
    return position.getX() == 0;
  }

  public static Boolean isPositionOnRightWall(Position position) {
    return position.getX() == getWidth() - 1;
  }

  public static Boolean isPositionOnUpperWall(Position position) {
    return position.getY() == 0;
  }

  public static Boolean isPositionOnUnderWall(Position position) {
    return position.getY() == getHeight() - 1;
  }

  public static void clearCells() {
    for (int x = 0; x < cells.length; x++)
      for (int y = 0; y < cells[0].length; y++)
        cells[x][y] = null;
  }

  public static void setFieldSizeFromString(String s) {
    String[] split = s.split("\\s");
    init(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
  }
}