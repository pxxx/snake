package gameLogic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.swing.*;

public class Game {
	public Snake[] snakes;
	public int heightScore = 0;

	private int numberOfPayers = 1;
	private int maxPointsAtField = 5;
	private boolean isSpawningPoints = false;

	private int rest;
	private int startRes = 500;
	private int speedUpAmount = 30;
	private int minRest = 30;
	private Timer continueTimer;

	public Game() {
		loadGameSettings();
		loadHeightScore();
		setValues();
		continueTimer.start();
	}

	private Boolean isItPossibleToSpawnAPoints() {
		return Field.countGameObject(GameObject.POINT) < maxPointsAtField;
	}

	public void continueGame() {
		for (Snake snake : snakes) {
			if (snake.isAlive) if (! snake.isColliding()) {
				collectPoints(snake);
				snake.move();
				if (snakes.length == 1) updateHeightScoreIfNecessary();
				if (isItPossibleToSpawnAPoints()) spawnPoints();
			} else {
				if (Arrays.stream(snakes).filter(it -> it.isAlive).count() <= 2) {
					setValues();
					break;
				} else snake.die(isSpawningPoints);
			}
		}
	}

	private void collectPoints(Snake snake) {
		if (snake.getNextHeadPosition().getGameObject() == GameObject.POINT) {
			snake.increaseLength();
			speedUp();
		}
	}

	private void updateHeightScoreIfNecessary() {
		if (snakes[0].getBlocks().size() > heightScore) heightScore = snakes[0].getBlocks().size();
	}

	private void spawnPoints() {
		Position freePosition = Field.getRandomFreePosition();
		if (freePosition != null) Field.cells[freePosition.getX()][freePosition.getY()] = GameObject.POINT;
	}

	private void setValues() {
		Field.clearCells();
		initSnakes();
		rest = startRes;
		if (continueTimer == null) continueTimer = new Timer(rest, t -> continueGame());
		else continueTimer.setDelay(rest);
	}

	private void speedUp() {
		if (rest > minRest) {
			rest -= speedUpAmount;
			if (rest < minRest) rest = minRest;
		}
		continueTimer.setDelay(rest);
	}

	private void loadGameSettings() {
		boolean isFieldSizeSet = false;

		try {
			BufferedReader br = new BufferedReader(new FileReader("res/GameSettings"));
			String line = br.readLine();
			while (line != null) {
				if (line.startsWith("fieldSize")) {
					Field.setFieldSizeFromString(line);
					isFieldSizeSet = true;
				} else if (line.startsWith("startRest")) {
					startRes = Integer.parseInt(line.split("\\s")[1]);
				} else if (line.startsWith("minRest")) {
					minRest = Integer.parseInt(line.split("\\s")[1]);
				} else if (line.startsWith("maxPointsAtField")) {
					maxPointsAtField = Integer.parseInt(line.split("\\s")[1]);
				} else if (line.startsWith("speedUpAmount")) {
					speedUpAmount = Integer.parseInt(line.split("\\s")[1]);
				} else if (line.startsWith("numberOfPayers")) {
					numberOfPayers = Integer.parseInt(line.split("\\s")[1]);
				} else if (line.startsWith("isSpawningPoints")) {
					isSpawningPoints = Boolean.parseBoolean(line.split(" ")[1]);
				}
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (! isFieldSizeSet) Field.init(25, 25);
	}

	private void initSnakes() {
		snakes = new Snake[numberOfPayers];
		for (int i = 0; i < snakes.length; i++) {
			snakes[i] = new Snake();
		}
	}

	private void loadHeightScore() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("res/heightScore"));
			heightScore = Integer.parseInt(br.readLine());
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			PrintWriter writer = new PrintWriter("res/heightScore");
			writer.println(heightScore + "");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void pause() {
		continueTimer.stop();
	}

	public void resume() {
		continueTimer.start();
	}
}