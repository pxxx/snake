package gameLogic;

import java.util.*;

public class Snake {
	private Direction direction;
	private final List<Position> blocks = new ArrayList<>();
	public boolean isAlive = true;

	public Snake() {
		setStartValues();
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public List<Position> getBlocks() {
		return blocks;
	}

	public int getLength() {
		return blocks.size();
	}

	private void setStartValues() {
		blocks.add(getStartPos());
		direction = Direction.random();
		addSnakeToTheField();
	}

	private Position getStartPos() {
		return Field.getCenter().getRandomPosition();
	}

	public void move() {
		removeSnakeFromField();
		moveBody();
		moveHead();
		addSnakeToTheField();
	}

	private void removeSnakeFromField() {
		Field.setCells(blocks, null);
	}

	private void addSnakeToTheField() {
		Field.setCells(blocks, GameObject.SNAKE);
	}

	public void moveBody() {
		for (int i = blocks.size() - 1; i > 0; i--)
			blocks.set(i, blocks.get(i - 1));
	}

	private void moveHead() {
		blocks.set(0, getNextHeadPosition());
	}

	public Position getNextHeadPosition() {
		Position head = getHeadPosition();
		head.moveOnes(direction);
		return head;
	}

	public Position getHeadPosition() {
		return blocks.get(0).copy();
	}

	public void increaseLength() {
		Position newPosition = getLastBlock();
		blocks.add(newPosition);
	}

	private Position getLastBlock() {
		return blocks.get(blocks.size() - 1);
	}

	public Boolean isColliding() {
		return isCollidingWithWall() || isCollidingWithSnake();
	}

	private boolean isCollidingWithSnake() {
		return getNextHeadPosition().getGameObject() == GameObject.SNAKE;
	}

	public void die(boolean isSpawningPoints) {
		Field.setCells(blocks, isSpawningPoints? GameObject.POINT: null);
		isAlive = false;
	}

	public Boolean isCollidingWithWall() {
		return isCollidingWithLeftWall() || isCollidingWithRightWall() || isCollidingWithUpperWall() || isCollidingWithUnderWall();
	}

	private Boolean isCollidingWithLeftWall() {
		return Field.isPositionOnLeftWall(getHeadPosition()) && getDirection() == Direction.LEFT;
	}

	private Boolean isCollidingWithRightWall() {
		return Field.isPositionOnRightWall(getHeadPosition()) && getDirection() == Direction.RIGHT;
	}

	private Boolean isCollidingWithUpperWall() {
		return Field.isPositionOnUpperWall(getHeadPosition()) && getDirection() == Direction.UP;
	}

	private Boolean isCollidingWithUnderWall() {
		return Field.isPositionOnUnderWall(getHeadPosition()) && getDirection() == Direction.DOWN;
	}
}