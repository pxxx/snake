package gameLogic;

import java.util.Random;

public enum Direction {
  DOWN, UP, LEFT, RIGHT;

  private static final Random RANDOM = new Random();

  public static Direction random() {
    return Direction.values()[RANDOM.nextInt(Direction.values().length - 1)];
  }

  public Direction getInvertDirection() {
    switch (this) {
    case DOWN:
      return Direction.UP;
    case UP:
      return Direction.DOWN;
    case LEFT:
      return Direction.RIGHT;
    case RIGHT:
    default:
      return Direction.LEFT;
    }
  }

  public static Random getRandom() {
    return RANDOM;
  }
}
