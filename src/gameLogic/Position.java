package gameLogic;

public class Position {

  private int x;
  private int y;

  public void setX(int x_) {
    x = x_;
  }

  public int getX() {
    return x;
  }

  public void setY(int y_) {
    y = y_;
  }

  public int getY() {
    return y;
  }

  public Position(int x_, int y_) {
    this.x = x_;
    this.y = y_;
  }

  public void moveOnes(Direction direction) {
    switch (direction) {
    case DOWN:
      y++;
      break;
    case UP:
      y--;
      break;
    case LEFT:
      x--;
      break;
    default:
      x++;
    }
  }

  public GameObject getGameObject() {
    return Field.cells[x][y];
  }

  public Boolean isFree() {
    return getGameObject() == null;
  }

  public Position copy() {
    return new Position(x, y);
  }
}