package ui.themeing;

import java.awt.Color;

public class Theme {
  public Color background;
  public Color textColor;
  public Color borderColor;
  public Color pointColor;
  public Color snakeColor;

  public Theme(Color background, Color textColor, Color borderColor, Color pointColor, Color snakeColor) {
    this.background = background;
    this.textColor = textColor;
    this.borderColor = borderColor;
    this.pointColor = pointColor;
    this.snakeColor = snakeColor;
  }

  public Theme(Color[] colors) {
    this.background = colors[0];
    this.textColor = colors[1];
    this.borderColor = colors[2];
    this.pointColor = colors[3];
    this.snakeColor = colors[4];
  }
}