package ui.themeing;

import javax.swing.JLabel;

public class Label extends JLabel {

  public Label(Theme theme) {
    super.setForeground(theme.textColor);
    super.setBackground(theme.background);
  }
}
