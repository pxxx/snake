package ui.themeing;

import javax.swing.JPanel;

public class Panel extends JPanel {
  protected Theme theme;

  public Panel(Theme theme) {
    this.theme = theme;
    this.setBackground(theme.background);
  }
}
