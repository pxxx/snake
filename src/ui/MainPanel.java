package ui;

import ui.themeing.Panel;
import ui.themeing.Theme;

import java.awt.*;

class MainPanel extends Panel {
	public InfoPanel infoPanel;
	public FieldPanel fieldPanel;

	public MainPanel(Theme theme) {
		super(theme);
		super.setLayout(new BorderLayout());
		setVisible(true);
		fieldPanel = new FieldPanel(theme);
		infoPanel = new InfoPanel(theme);
		infoPanel.setPreferredSize(calcPreferredSize());
		super.add(fieldPanel, BorderLayout.CENTER);
		super.add(infoPanel, BorderLayout.SOUTH);
		super.add(generateSpaceHolder(), BorderLayout.WEST);
		super.add(generateSpaceHolder(), BorderLayout.EAST);
	}

	private Panel generateSpaceHolder() {
		Panel spaceHolder = new Panel(theme);
		spaceHolder.setPreferredSize(calcPreferredSize());
		return spaceHolder;
	}

	private Dimension calcPreferredSize() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		return new Dimension((int) ((screenSize.getWidth() - fieldPanel.getPreferredSize().getWidth())/2), (int) (screenSize.getHeight() - fieldPanel.getPreferredSize().getHeight()));
	}
}