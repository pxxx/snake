package ui;

import gameLogic.Game;
import gameLogic.Direction;
import ui.themeing.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;

class Gui extends JFrame {
  /**Game: contains the hole game logic */
  private final Game game = new Game();
  private Theme theme;
  private final MainPanel mainPanel;

  WindowListener exitListener = new WindowAdapter() {
    @Override
    public void windowClosing(WindowEvent e) {
      exit();
    }
  };

  private final KeyAdapter keyAdapter = new KeyAdapter() {
    @Override
    public void keyPressed(KeyEvent e) {
      if (e.getModifiersEx() == KeyEvent.CTRL_DOWN_MASK) {
        if (e.getKeyCode() == KeyEvent.VK_Q) exit();
      } else {
        moveKeysPress(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
          pause();
        }
      }
    }
  };

  private void moveKeysPress(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_A:
        game.snakes[0].setDirection(Direction.LEFT);
        break;
      case KeyEvent.VK_D:
        game.snakes[0].setDirection(Direction.RIGHT);
        break;
      case KeyEvent.VK_S:
        game.snakes[0].setDirection(Direction.DOWN);
        break;
      case KeyEvent.VK_W:
        game.snakes[0].setDirection(Direction.UP);
        break;

    case KeyEvent.VK_H:
      game.snakes[ game.snakes.length -1 ].setDirection(Direction.LEFT);
      break;
    case KeyEvent.VK_L:
      game.snakes[ game.snakes.length -1 ].setDirection(Direction.RIGHT);
      break;
    case KeyEvent.VK_J:
      game.snakes[ game.snakes.length -1 ].setDirection(Direction.DOWN);
      break;
    case KeyEvent.VK_K:
      game.snakes[ game.snakes.length -1 ].setDirection(Direction.UP);
      break;

      case KeyEvent.VK_LEFT:
        game.snakes[ (game.snakes.length == 1) ? 0 : 1 ].setDirection(Direction.LEFT);
        break;
      case KeyEvent.VK_RIGHT:
        game.snakes[ (game.snakes.length == 1) ? 0 : 1 ].setDirection(Direction.RIGHT);
        break;
      case KeyEvent.VK_UP:
        game.snakes[ (game.snakes.length == 1) ? 0 : 1 ].setDirection(Direction.UP);
        break;
      case KeyEvent.VK_DOWN:
        game.snakes[ (game.snakes.length == 1) ? 0 : 1 ].setDirection(Direction.DOWN);
        break;
    }
  }

  public Gui() {
    loadGuiSettings();
    mainPanel = new MainPanel(theme);
    super.setLocationRelativeTo(null);
    super.addWindowListener(exitListener);
    super.setTitle("Snake");
    super.setLocationRelativeTo(null);
    super.setExtendedState(JFrame.MAXIMIZED_BOTH);
    super.setUndecorated(true);
    super.setVisible(true);
    super.getContentPane().add(mainPanel);
    mainPanel.fieldPanel.addKeyListener(keyAdapter);

    super.revalidate();
    super.repaint();
    Timer updateGueElementsTimer = new Timer(10, t -> updateGuiElements());
    updateGueElementsTimer.start();

    mainPanel.fieldPanel.grabFocus();
  }

  private void updateGuiElements() {
    mainPanel.fieldPanel.repaint();
    if (game.snakes.length == 1) {
      mainPanel.infoPanel.setLengthLabelText(game.snakes[0].getLength());
      mainPanel.infoPanel.setHeightScoreLabelText(game.heightScore);
    }
  }

  private void pause() {
    game.pause();
    int continueGame = JOptionPane.showOptionDialog(
            null, "continue", "continue", JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE, null, null, null
    );
    if (continueGame == 0)
      game.resume();
    else
      exit();
  }

  private void exit() {
    game.save();
    System.exit(0);
  }

  private void loadGuiSettings() {
    // set default values those will be overridden if there are set in the config file
    Color[] colors = { Color.BLACK, Color.GREEN, Color.gray, Color.BLUE, Color.RED };

    try {
      BufferedReader br = new BufferedReader(new FileReader("res/GuiSettings"));
      String line = br.readLine();
      while (line != null) {
        String[] split = line.split(" ");
        if (line.startsWith("background")) {
          colors[0] = new Color(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
        } else if (line.startsWith("textColor")) {
          colors[1] = new Color(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
        } else if (line.startsWith("borderColor")) {
          colors[2] = new Color(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
        } else if (line.startsWith("pointColor")) {
          colors[3] = new Color(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
        } else if (line.startsWith("snakeColor")) {
          colors[4] = new Color(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
        } else if (line.startsWith("cellSize")) {
          FieldPanel.cellSize = Integer.parseInt(split[1]);
        }
        line = br.readLine();
      }
      br.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    theme = new Theme(colors);
  }

  public static void main(String[] args) {
    new Gui();
  }
}