package ui;

import gameLogic.Field;
import gameLogic.GameObject;
import ui.themeing.Panel;
import ui.themeing.Theme;

import java.awt.*;
import javax.swing.*;

class FieldPanel extends Panel {
  public static int cellSize = 25;

  public FieldPanel(Theme theme) {
    super(theme);
    super.setVisible(true);
    super.setBorder(BorderFactory.createLineBorder(super.theme.borderColor));
    super.setPreferredSize(new Dimension(Field.getWidth() * cellSize, Field.getHeight() * cellSize));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    for (int x = 0; x < Field.getWidth(); x++)
      for (int y = 0; y < Field.getHeight(); y++) {
        g.setColor(selectColorForGameObject(Field.cells[x][y]));
        g.fillRect(cellSize * x, cellSize * y, cellSize, cellSize);
      }
  }

  private Color selectColorForGameObject(GameObject object) {
    return (object == GameObject.SNAKE) ? super.theme.snakeColor
        : (object == GameObject.POINT) ? super.theme.pointColor : super.theme.background;
  }
}