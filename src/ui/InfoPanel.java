package ui;

import ui.themeing.*;

public class InfoPanel extends Panel {
  private final Label lengthLabel;
  private final Label heightScoreLabel;

  public InfoPanel(Theme theme) {
    super(theme);
    super.setVisible(true);
    lengthLabel = new Label(theme);
    heightScoreLabel = new Label(theme);
    super.add(lengthLabel);
    super.add(heightScoreLabel);
  }

  public void setLengthLabelText(int length) {
    lengthLabel.setText("length: " + length);
  }

  public void setHeightScoreLabelText(int heightScore) {
    heightScoreLabel.setText("heightScore: " + heightScore);
  }
}
