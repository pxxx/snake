# Allgemeines

<p> Hier bei handelt es sich um eine anpassbare Implementierung von Snake. <\p>

<p> Der "jar-file" befindet sich unter out/artifacts/Snake_jar/Snake.jar <\p>

# Nutzerhandbuch

<p> Im Folgenden wird aufgeführt, wie das Program verwendet werden kann. <\p>

## Steuerung

<p> Das einsigste, was angesteuert werden kann ist die Richtung in die sich die Schlange bewegen soll. <\p>

### Hier zu gibt es drei Setz an Tasten: 
- W(hoch), A(links), S(runter), D(rechts) <br>
  -> Egal wieviele Spieler es gibt, kann mit den forangegengenen Tasten die erste Schlacnge gesteuert werden.

- H(liks), J(runter), K(hoch), L(rechts) <br>
  -> Mit diesem von vim bzw. vi stammenden Tastenset läste sich bei einem Spiele der selbige und ansonsten die zweite Schlange steuern.

- Pfeiltasten (richtung der Schlange entsprich richtung der Pfeile) <br>
  -> Steuert bei einer Schlange die selbige, bei zweien die Zwete und bei dreien die drite.

<p> * Außerdem kann mit Str-Q die Anwendung geschlossen werden wobei automatich der Heightscore gespeichert wird. Mit ESC kann das Speil pausirt werden. Babe popt ein Dialog auf, mit dessen  mit  „Yes“ beschrifteten Knopf das spiel fordgeführt wird und der mit „Nein“ beschriftete schließt das Spiel. <\p>

### Einstellungen
<p> Es gibt zwei Arten der Einstellungen. Zum einen die das Spiel unmitelbar betreffenden und zum anderen jene die sich nur auf die Benuzeroberfläche auswirken. Änderungen werden mittels 
Config-Files vorgennomen. Außerdem gibt es für all diese Einstellungen Standardwerte, welch dierekt im Code stehen, es ist also ach problemlos möglich die Dateien einfach leer zu lassen. 
Die einselnen Einstelungsoptionen werden immer von einem Schlüsselwort eingeleitet. Darauf folgen dan duch Leerzeichen seperiert die Werte auf welche sie gesetzt werden soll. Alle zeilen, die nicht durh ein Schlüsselwort eingeleitet werden, werden von dem Programm einfach ignoriert. <\p>

##### Beispeile:

- speedUpAmount 30
- background 23 34 0

#### Spieleeinstelungen

<p> Die Spieleeinstelungen sind unter „res/GameSettings“ verzeichnet. <\p>

##### Folgende Einstellungen lassen sich hier tätigen: 

- Speilfeldgröße: <br>
Schlüsselwort = fieldSize <br>
Dannach kommen zwei Ganzzahlwerte. Der Erste für die Breite und die Zweite für die Höhe.

- Wie lange zu Beginn zwischem Fortführen des Spiels gewartet wirt: <br>
Schlüsselwort =  startRest <br>
Gefolgt von einen  Ganzzahlwerte welche die Zeit in Millisekunden angibt. <br>

- wie niedrig diese Zeitspanne höchstens werden kann: <br>
Schlüsselwort = minRest <br>
Daraufhin ein  Ganzzahl für die Zeit in Millisekunden. <br>

- um welche Zeitspanne sich diese Wartezeit beim einsammeln eines Punkts veringern soll: <br>
Schlüsselwort = speedUpAmount <br>
Dahinter ein  Ganzzahl für die Zeit in Millisekunden. <br>

- wieviele Punkte höchstens gleichzeitig auf dem Spelfeld sein können: <br>
Schlüsselwort = maxPointsAtField <br>
Bekleidet durch eine Ganzzahl für die maximale Anzahl. <br>

- Anzahl Spieler: <br>
Schlüsselwort = numberOfPayers <br>
Gefolgt durch eine Ganzzahl, welche die Anzahl an Spielern darstellt. <br>


#### GUI-Einstellungen 

<p> In diesen Einstellungen lassen sich im Grunde genommen zwei Dienge einstellen. Zum einen die Farben und zum anderen die Größe der quatratichen Zellen onder Kacheln des Spielfeldes.  Bei den Farben folgen auf das Schlüsselwort drei Ganzzahlen für den RGB-Werte der Farbe. <p>

##### Folgende Farben lassen sich fest legen: 

- Hindergrunffarbe: background 
- Textfarbe: textColor 
- Farbe von Umrandungen: borderColor 
- Farbe der Punkte: pointColor 
- Fabe der Schlange: snakeColor 

<p> Die Größe der Spielfeldkachel wird durch einen einen Ganzahlwert für Höhe und Breite in Pixeln festgelegt. Schlüsselwort is cellSize. <\p>
